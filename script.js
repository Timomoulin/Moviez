let inputRecherche = document.querySelector("#recherche");

async function uneFonctionFetch(url) {
    let APIKey = "&apikey=22b0b810";
    let reponse = await fetch(url + APIKey);
    let data = await reponse.json();
    // console.log(data);
    return data;
}

function affiche(desFilms) {
    let divFilms = document.querySelector("#films");
    let chaineHTML = "";
    for (const unFilm of desFilms) {
        chaineHTML += (`<div class="card bg-secondary border-warning text-light" style="width: 18rem;">
    
    <img src="${unFilm.Poster}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">${unFilm.Title} (${unFilm.Year})</h5>
      <p class="card-text"></p>
    </div>
  </div>`)
    }
    divFilms.innerHTML = chaineHTML;
}

uneFonctionFetch("http://www.omdbapi.com/?s=batman").then(function (resultat) {
    console.log(resultat)
})

inputRecherche.addEventListener("keyup", async function () {
    let titreRecherche = inputRecherche.value;
    let objetRecherche = await uneFonctionFetch("http://www.omdbapi.com/?s=" + titreRecherche);
    // console.log(objetRecherche);
    if (objetRecherche.Reponse = "True") {
        console.log(objetRecherche.Search)
        affiche(objetRecherche.Search);
    }
})